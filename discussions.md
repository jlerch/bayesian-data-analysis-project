# Discussions
Here, results of discussions will be written down that are important but are not to be mentioned in the report itself.

## Initial Proposal
The data to be investigated is the [octoberfest dataset](https://www.kaggle.com/datasets/prajwaldongre/october-fest-from-1985-2022). Our goal is to determine the price of the beer and the chicken per unit to maximize income (i.e. `price * consumed units`) for the next year.

## Interview with TA
Regarding our proposal, we asked several questions to the teaching staff.

### Is the dataset suitable?
Yes. It might be an issue if it is too large or too small. In our case: If it is too small that is okay, it just has to be reported in the end. You can also use it to create reasonable models for it.

### What kinds of models can be used?
The simplest one would be a normal linear regression model. The covariates (inputs) can be year, chicken price, and beer price.

The model can be created so that the number of consumed beers and number of consumed chickens are written in a vector. This vector could be distributed following a multivariate Gaussian. (In that way you can also look into interdependencies.)

A spontaneous sketch of the model looks like this:
$$
u \sim N(B_tt + B_cc + B_0, \sigma),
$$
where $u$ is the number of consumed units, $t$ is the number of the year, $c$ is the price (cost) of one unit. The parameters could be matrices looking like this:
$$
B = 
\begin{pmatrix}
\beta_0 & 0\\
0 & \beta_1
\end{pmatrix}.
$$
$B$ is a diagonal matrix, where each diagonal element is the corresponding parameter for consumed units of chicken and beer respectively.

One could change this model to have the profit directly as an output variable. This depends on which optimization method we use (see below).

This model is not hierarchical. To include hierarchicity, one has to consider some grouping. In this case, this is a little bit tricky and the dataset might not be suited well for this, but it can be used nevertheless. In the worst case one could find that the hierarchical model just does not work that well. One possible option would be to 

1. group the number of visitors following some range to retrieve groups of ranges (300-350, 350-400, etc.).
2. group the consumed units in respect to the range of visitors.

Then one could assign different parameters $B$ for the  different groups. Those parameters themselves follow a distribution which itself is parameterized by a variable that follows a hyperprior.

Furthermore, one could extend this model to be nonlinear.

### How to optimize the model?
Our proposal wasn't that bad: Use the posterior predictive distribution to generate multiple distributions of consumed units with multiple prices as input and choose that combination of consumption and price that yields the highest profit.

Alternative approaches are also possible. The keyword here is "Bayesian Optimization", which is not in the scope of this course. However, we could ask Aki if he can propose us an optimization method.

### How to do something like regression analysis (ANOVA etc.) in a bayesian way?
Example: Is the parameter significantly different from zero?
> Given you know the posterior, then you can calculate the probability of a parameter being greater than zero. If that probability is high enough (say greater than 95%) you could conclude something like "yeeeaa, seems legit".

> However, there is an alternative approach which is called "variable selection" and it should be teached later in the course.

## Hints for the presentation

- he presents a template

intro
- introduce yourself
- ... 
- introduce topic motivation
- show visualization of plain data, explain content
    - visualize simple preprocessing tasks
- plots
    - thick enough lines
    - coloblind proof (package "palette")
    - use direct labeling in graphics instead of legends, 

- model visualization
    - show brms formulas
    - show model fits

- ppc
    - marginally
    - condituonally
    - loo checking, loo-pit checking
    - show R2 values, elpd difference

- how well to make predictions to something not yet seen?
    - hold out test set
    - how does year 2023 compare to others? (explain what you find)

- convergence diagnostics
    - "mcmc sampling performedwell based on the usual convergence diagnostics"
    - if problem in convergence diagnostics: explain that, why they occured, what we did to cope with that

- conclusions 
    - "latent hierarchical model with spline can mdoel ... of the ..."

extra hits:
    - fonts and figures should be big enough!: theme_set(... base_size=...))
    - you may present equations, but you should think about: is this equation needed? you do not have much time!
    - end presentation with: not the thanks slides, but END WITH CONCLUSION SLIDE AND CONTACT INFORMATION!
    - additional slides after "conclusion" slides

## Notes on Sampling Quality
The following things are to be considered when looking at sampling process
- trace plots
    - mixing
    - if many variables, this is hard -> use rhat instead
    - between-chain var should be lower than within chain var
    - rhat can fail
    - **improved by** increasing number of iterations
- effective sample size
    - number of independent samples as determined from the amount of increase in uncertainty on posterior estimates due to correlated samples
    - neff/iterations should be > 0.01
    - ess over 100 per chain is considered good
    - difference between bulk_ess and tail_ess? -> bulk_ess tells about bulk of dist, tail_ess about tails of dist 
    - **improved by** running longer iterations and thinning chains (throwing away correlated samples)
- divergence 
    - sometimes can be false positives.
        - **improved by** smaller step size (eta) equivalent to larger adapt_delta
    - otherwise: **improved by** reparameterize model or change priors